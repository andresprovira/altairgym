﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Monitor : Person
    {
        [InverseProperty("Monitor")]
        public List<Activity> Activities { get; set; }
    }
}
