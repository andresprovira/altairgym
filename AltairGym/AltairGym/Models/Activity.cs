﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Activity
    {
 

        public int ID { get; set; }
       
        [Required(ErrorMessage ="Este campo es requerido")]
        public string Tittle { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage = "Maximo 50 character")]
        public string Description { get; set; }
        
        public int NumberAssistants { get; set; }
        public int Duration { get; set; }
        public Boolean Active { get; set; }

        public int MonitorID { get; set; }
        [ForeignKey("MonitorID")]
        public Monitor Monitor { get; set; }
        [InverseProperty("Activity")]
        public List<ClassRoom> ClassRooms { get; set; }


    }
}
