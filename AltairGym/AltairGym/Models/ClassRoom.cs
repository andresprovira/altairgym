﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class ClassRoom
    {
        public int ID { get; set; }
        [DataType(DataType.Date)]
        [Required]
        public DateTime DateClass { get; set; }
        [DataType(DataType.Time)]
        [Required]
        public DateTime Hour { get; set; }

        public int RoomID { get; set; }

        [ForeignKey("RoomID")]
        public Room Room { get; set; }

        public int ActivityID { get; set; }
        [ForeignKey("ActivityID")]
        public Activity Activity { get; set; }

        [InverseProperty("ClassRoom")]
        public List<Booking> Bookings { get; set; }

    }
}
