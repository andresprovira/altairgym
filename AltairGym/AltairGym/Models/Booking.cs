﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Booking
    {
        public int ID { get; set; }
        [DataType(DataType.DateTime)]
        [Required]
        public DateTime BookingDate { get; set; }

        public int CustomerID { get; set; }
        [ForeignKey("CustomerID")]
        public Customer customer { get; set; }

        public int ClassRoomsID { get; set; }

        [ForeignKey("ClassRoomsID")]
        public ClassRoom ClassRoom { get; set; }

    }
}
