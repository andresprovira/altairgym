﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public abstract class Person
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Nombre máximo de 20 carácteres")]
        public string Name { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Must be between 1 and 30 characters", MinimumLength = 1)]
        public string Surname { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        [Phone]
        public string Phone { get; set; }
        [Required]
        [Range(18, 65, ErrorMessage = "The number must be between 18 and 65")]
        public int Edad { get; set; }

        [Required]
        public string Nick { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        public string Rol { get; set; }

        


    }
}
