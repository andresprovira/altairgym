﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Models
{
    public class Room
    {
        public int ID { get; set; }

        [StringLength(30)]
        [Required]
        public string Name { get; set; }

        [Range(1, 30, ErrorMessage = "The number must be between 1 and 30")]
        public int Capacity { get; set; }

        
        public List<ClassRoom> ClassRooms { get; set; }
    }
}
