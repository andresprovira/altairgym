﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AltairGym.Data;
using AltairGym.Models;

namespace AltairGym.Controllers
{
    public class ActivitiesController : Controller
    {
        private readonly GymContext _context;

        public ActivitiesController(GymContext context)
        {
            _context = context;
        }

        // GET: Activities
        public async Task<IActionResult> Index()
        {
            var gymContext = _context.Activities.Include(a => a.Monitor);
            return View(await gymContext.ToListAsync());
        }

        // GET: Activities/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activity = await _context.Activities
                .Include(a => a.Monitor)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (activity == null)
            {
                return NotFound();
            }

            return View(activity);
        }

        // GET: Activities/Create
        public IActionResult Create()
        {
            ViewData["MonitorID"] = new SelectList(_context.Monitors, "ID", "Name");
            return View();
        }

        // POST: Activities/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Tittle,Description,NumberAssistants,Duration,Active,MonitorID")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                _context.Add(activity);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MonitorID"] = new SelectList(_context.Monitors, "ID", "Name", activity.MonitorID);
            return View(activity);
        }

        // GET: Activities/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            /*Para poder marcar como inactiva una actividad no puede tener una clase futura, es
            decir, una clase con fecha igual o mayor a la fecha del sistema.*/
            //Capturo activity obteniendo la id que recibe en el get del edit
            var activity = await _context.Activities.FindAsync(id);

            //Obtengo una lista de clases ya que puede haber clases futuros con la misma activida y la filtro porque la fecha sea mayor que la del sistema

            var classRooms = _context.ClassRooms.Where(c => c.ActivityID == activity.ID && c.DateClass >= DateTime.Now).ToList();
            //Guardo el número de clases en un viewbag para hacer la condición el la vista
            ViewBag.classRooms = classRooms.Count;
            if (activity == null)
            {
                return NotFound();
            }
            ViewData["MonitorID"] = new SelectList(_context.Monitors, "ID", "Name", activity.MonitorID);
            return View(activity);
        }

        // POST: Activities/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Tittle,Description,NumberAssistants,Duration,Active,MonitorID")] Activity activity)
        {
            //Capturo la lista de clases que tiene esta actividad en fechas posteriores a la actual
            var classRooms = _context.ClassRooms.Where(c => c.ActivityID == activity.ID && c.DateClass >= DateTime.Now).ToList();
            //Guardo en un viewbag la cantidad de la lista
            ViewBag.classRooms = classRooms.Count;

            //si la lista no está vacía, significa que no puede cambiarse el atributo active y lo ponemos a true
            if (ViewBag.classRooms != 0)
            {
                activity.Active = true;
            }

            if (id != activity.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(activity);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActivityExists(activity.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MonitorID"] = new SelectList(_context.Monitors, "ID", "Name", activity.MonitorID);
            return View(activity);
        }

        // GET: Activities/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var activity = await _context.Activities
                .Include(a => a.Monitor)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (activity == null)
            {
                return NotFound();
            }

            return View(activity);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var activity = await _context.Activities.FindAsync(id);
            _context.Activities.Remove(activity);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ActivityExists(int id)
        {
            return _context.Activities.Any(e => e.ID == id);
        }
    }
}
