﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AltairGym.Data;
using AltairGym.Models;

namespace AltairGym.Controllers
{
    public class ClassRoomsController : Controller
    {
        private readonly GymContext _context;

        public ClassRoomsController(GymContext context)
        {
            _context = context;
        }

        // GET: ClassRooms
        public async Task<IActionResult> Index()
        {   //Incluimos Bookings para en la vista poder ver si esa clase tiene reservas o no
            var gymContext = _context.ClassRooms.Include(c => c.Activity).Include(c => c.Room).Include(c=>c.Bookings);

            return View(await gymContext.ToListAsync());
        }

        /*Con este método listo las clases disponibles en el sistema*/
        public async Task<IActionResult> BuscarFecha() //Este método nos llevará a la vista "BuscarFecha"
        {
            //El Where contempla el punto 4 de funcionalidad

            /*Para la inscripción en una clase, la fecha de la clase debe
            ser posterior y el número de reservas no puede ser igual o mayor al número máximo
        de asistentes.*/

            //Obtengo la lista de clases y con el where creo la condifición que solo obtenga las fechas igual o posterior a la fecha del sistema y que no se haya completado el aforo
            var altairGymContext = _context.ClassRooms.Include(c => c.Activity).Include(c => c.Room).Include(c => c.Bookings).Where(c => c.DateClass >= DateTime.Now && c.Activity.NumberAssistants <= c.Room.Capacity);
            return View(await altairGymContext.ToListAsync());
        }

        // GET: ClassRooms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classRoom = await _context.ClassRooms
                .Include(c => c.Activity)
                .Include(c => c.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (classRoom == null)
            {
                return NotFound();
            }

            return View(classRoom);
        }

        // GET: ClassRooms/Create
        public IActionResult Create()
        {
            ViewData["ActivityID"] = new SelectList(_context.Activities, "ID", "Tittle");
            ViewData["RoomID"] = new SelectList(_context.Rooms, "ID", "Name");
            return View();
        }

        // POST: ClassRooms/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,DateClass,Hour,RoomID,ActivityID")] ClassRoom classRoom)
        {
            if (ModelState.IsValid)
            {
                // si el modelo es válido obtenemos la actividad en cuestión que donde la id se encuentre en la lista de classrooms
                var activity = _context.Activities.FirstOrDefault(a => a.ID == classRoom.ActivityID);
                //Comprobamos si la propiedad active de activity está activo o no
                if (!activity.Active)
                {
                    //En caso de que no lo esté creamos un mensaje de error para el usuario 
                    TempData["errorMessage"] = "no puedes crear una clase con una actividad inactiva";
                    //Lo redirigimos al Create, para que vuelva a realizar el formulario
                    return RedirectToAction(nameof(Create));
                }
                _context.Add(classRoom);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ActivityID"] = new SelectList(_context.Activities, "ID", "Tittle", classRoom.ActivityID);
            ViewData["RoomID"] = new SelectList(_context.Rooms, "ID", "Name", classRoom.RoomID);
            return View(classRoom);
        }

        // GET: ClassRooms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classRoom = await _context.ClassRooms.FindAsync(id);
            if (classRoom == null)
            {
                return NotFound();
            }
            ViewData["ActivityID"] = new SelectList(_context.Activities, "ID", "Tittle", classRoom.ActivityID);
            ViewData["RoomID"] = new SelectList(_context.Rooms, "ID", "Name", classRoom.RoomID);
            return View(classRoom);
        }

        // POST: ClassRooms/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,DateClass,Hour,RoomID,ActivityID")] ClassRoom classRoom)
        {
            if (id != classRoom.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(classRoom);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClassRoomExists(classRoom.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ActivityID"] = new SelectList(_context.Activities, "ID", "Tittle", classRoom.ActivityID);
            ViewData["RoomID"] = new SelectList(_context.Rooms, "ID", "Name", classRoom.RoomID);
            return View(classRoom);
        }

        // GET: ClassRooms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var classRoom = await _context.ClassRooms
                .Include(c => c.Activity)
                .Include(c => c.Room)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (classRoom == null)
            {
                return NotFound();
            }

            return View(classRoom);
        }

        // POST: ClassRooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var classRoom = await _context.ClassRooms.FindAsync(id);
            _context.ClassRooms.Remove(classRoom);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ClassRoomExists(int id)
        {
            return _context.ClassRooms.Any(e => e.ID == id);
        }
    }
}
