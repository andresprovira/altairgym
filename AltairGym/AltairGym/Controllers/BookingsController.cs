﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AltairGym.Data;
using AltairGym.Models;

namespace AltairGym.Controllers
{
    public class BookingsController : Controller
    {
        private readonly GymContext _context;

        public BookingsController(GymContext context)
        {
            _context = context;
        }

        // GET: Bookings
        public async Task<IActionResult> Index()
        {
            var gymContext = _context.Bookings.Include(b => b.ClassRoom).Include(b => b.customer);
            return View(await gymContext.ToListAsync());
        }

        // GET: Bookings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings
                .Include(b => b.ClassRoom)
                .Include(b => b.customer)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // GET: Bookings/Create
        public IActionResult Create()
        {
            ViewData["ClassRoomsID"] = new SelectList(_context.ClassRooms, "ID", "ID");
            ViewData["CustomerID"] = new SelectList(_context.Customers, "ID", "Name");
            return View();
        }

        // POST: Bookings/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,BookingDate,CustomerID,ClassRoomsID")] Booking booking)
        {
            
            

            if (ModelState.IsValid)
            {
                //Funcionalidad la fecha de la clase debe ser posterior
                //Para ello obtengo la clase a través de booking.ClassRoomsID
                var classRoom = _context.ClassRooms.Include(a=>a.Activity).FirstOrDefault(c => c.ID == booking.ClassRoomsID);
                //Aqui obtengo la lista de bookings con la clase que he seleccionado.
                //Obtengo la cantidad de elementos de la lista con .count
                var bookings = _context.Bookings.Where(b=>b.ClassRoomsID==classRoom.ID).ToList(); // número de reservas
                //La fecha de la clase debe ser posterior a la reserva
                if (booking.BookingDate <= classRoom.DateClass || bookings.Count>=classRoom.Activity.NumberAssistants)
                {
                    //Personalizo el mensaje de error
                    if (booking.BookingDate <= classRoom.DateClass)
                    {
                        TempData["errorMessage"] = "La fecha de la reserva no puede ser anterior a la fecha de la clase";
                    } 
                    
                    if(bookings.Count >= classRoom.Activity.NumberAssistants)
                    {
                        TempData["errorMessage"] = " Hay mas reservas que número de asistentes";
                    }
                   
                    //Redirigimos al create
                    return RedirectToAction(nameof(Create));
                }

                    _context.Add(booking);

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClassRoomsID"] = new SelectList(_context.ClassRooms, "ID", "ID", booking.ClassRoomsID);
            ViewData["CustomerID"] = new SelectList(_context.Customers, "ID", "Name", booking.CustomerID);
            return View(booking);
        }

        // GET: Bookings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }
            ViewData["ClassRoomsID"] = new SelectList(_context.ClassRooms, "ID", "ID", booking.ClassRoomsID);
            ViewData["CustomerID"] = new SelectList(_context.Customers, "ID", "Name", booking.CustomerID);
            return View(booking);
        }

        // POST: Bookings/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,BookingDate,CustomerID,ClassRoomsID")] Booking booking)
        {
            if (id != booking.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(booking);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookingExists(booking.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClassRoomsID"] = new SelectList(_context.ClassRooms, "ID", "ID", booking.ClassRoomsID);
            ViewData["CustomerID"] = new SelectList(_context.Customers, "ID", "Name", booking.CustomerID);
            return View(booking);
        }

        // GET: Bookings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var booking = await _context.Bookings
                .Include(b => b.ClassRoom)
                .Include(b => b.customer)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (booking == null)
            {
                return NotFound();
            }

            return View(booking);
        }

        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var booking = await _context.Bookings.FindAsync(id);
            _context.Bookings.Remove(booking);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookingExists(int id)
        {
            return _context.Bookings.Any(e => e.ID == id);
        }
    }
}
