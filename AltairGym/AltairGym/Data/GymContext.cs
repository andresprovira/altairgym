﻿using AltairGym.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltairGym.Data
{
    public class GymContext : DbContext
    {

        public GymContext(DbContextOptions<GymContext> options) : base(options)
        {
        }

        public DbSet<Activity> Activities { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<ClassRoom> ClassRooms { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Monitor> Monitors { get; set; }
        public DbSet<Room> Rooms { get; set; }

    }


}
